FROM openjdk:16

WORKDIR /app/backend

COPY *.jar app.jar

ENTRYPOINT ["java","-jar","/app/backend/app.jar"]