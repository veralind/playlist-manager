# Playlist Manager for Spotify

### https://playlistmanager.10cats.com

Playlist Manager is a Spotify plugin for getting new tracks from the playlists you follow!

## How does it work?
1. Log in with your Spotify account
2. Select the playlists you want to keep track of ;)
3. Your very own private playlist will be created for you in your Spotify
4. All the new tracks added to chosen playlists will end up in it

It's that simple! 

## What makes it work?
* Java
* Spring Boot
* PostgreSQL
* React
* JUnit5 & Mockito
* CI/CD via gitlab: 
   * automated build
   * Docker containerization
   * deployment in Kubernetes
* scheduled cronjob

## Demo video
![](playlistmanager.mp4)