import React from "react";
import { welcomeText } from "./../res/Text";

const WelcomeTextBlock = () => {
  return <div>{welcomeText}</div>;
};

export default WelcomeTextBlock;
