const express = require('express');
const bodyParser = require('body-parser');
const path = require('path')
const app = express();
const port = 8081;
const { createProxyMiddleware } = require('http-proxy-middleware');

app.get('/playlists', (req, res, next) => {
  req.url = '/';
  next();
});

app.use(express.static(path.join(__dirname, 'client/build')));

app.use('/auth', createProxyMiddleware({ target: 'http://backend:8080', changeOrigin: true}));

app.use('/allplaylists', createProxyMiddleware({ target: 'http://backend:8080', changeOrigin: true}));

app.use('/chosenplaylists', createProxyMiddleware({ target: 'http://backend:8080', changeOrigin: true}));

app.use('/upd', createProxyMiddleware({ target: 'http://backend:8080', changeOrigin: true}));

app.use('/update', createProxyMiddleware({ target: 'http://backend:8080', changeOrigin: true}));

app.listen(port, () => console.log(`Listening on port ${port}`));

//exit on ctrl+c
process.on('SIGINT', function() {
        process.exit();
});
