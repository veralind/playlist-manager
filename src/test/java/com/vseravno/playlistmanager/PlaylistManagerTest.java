package com.vseravno.playlistmanager;

import com.google.common.collect.Lists;
import com.vseravno.playlistmanager.PlaylistManager;
import com.vseravno.playlistmanager.PlaylistObject;
import com.vseravno.playlistmanager.TrackObject;
import com.vseravno.playlistmanager.api.SpotifyClient;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import org.assertj.core.util.Sets;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PlaylistManagerTest {
  @Mock SpotifyClient client;

  @Test
  public void updateTargetPlaylistTest() {

    HashSet<TrackObject> tracks = new HashSet<>();
    for (int i = 0; i < 300; i++) {
      TrackObject trackObject = TrackObject.create("track" + i, "playlistSid", "trackName");
      tracks.add(trackObject);
    }

    when(client.getUserPlaylists()).thenReturn(new ArrayList<PlaylistSimplified>());

    new PlaylistManager(client).updateTargetPlaylist("playlistSid", tracks);

    Mockito.verify(client)
        .addTracksToPlaylist("playlistSid", Lists.newArrayList(tracks).subList(0, 100));
    Mockito.verify(client)
        .addTracksToPlaylist("playlistSid", Lists.newArrayList(tracks).subList(100, 200));
    Mockito.verify(client)
        .addTracksToPlaylist("playlistSid", Lists.newArrayList(tracks).subList(200, 300));
  }
}
