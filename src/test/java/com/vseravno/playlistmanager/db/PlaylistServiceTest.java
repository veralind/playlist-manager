package com.vseravno.playlistmanager.db;

import com.vseravno.playlistmanager.PlaylistObject;
import com.vseravno.playlistmanager.api.UserAuth;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PlaylistServiceTest {
  @Mock PlaylistRepository playlistRepository;

  @Mock AccountService accountService;

  @Test
  void updatePlaylistsTest() throws Exception {
    UserAuth userAuth = new UserAuth("userSid", "AT", "RT");
    List<Playlist> dbPlaylists =
        List.of(
            new Playlist(1, "sid1", "snap1", Timestamp.from(Instant.EPOCH)),
            new Playlist(1, "sid2", "snap2", Timestamp.from(Instant.EPOCH)));

    List<PlaylistObject> chosenPlaylists =
        List.of(
            PlaylistObject.create("sid2", "name2", "snap2", null, null, null),
            PlaylistObject.create("sid3", "name3", "snap3", null, null, null));

    when(accountService.getAccountId(userAuth)).thenReturn(1L);

    PlaylistService playlistService = new PlaylistService(playlistRepository,accountService);
    playlistService.updatePlaylists(chosenPlaylists,userAuth,dbPlaylists);

    verify(playlistRepository).delete(dbPlaylists.get(0));
    verify(playlistRepository)
        .save(new Playlist(1, "sid3", "snap3", Timestamp.from(Instant.EPOCH)));
  }
}
