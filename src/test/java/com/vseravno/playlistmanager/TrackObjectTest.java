package com.vseravno.playlistmanager;

import com.vseravno.playlistmanager.TrackObject;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TrackObjectTest {

  @Test
  public void create() {
    HashSet<TrackObject> trackObjects = new HashSet<>();

    trackObjects.add(TrackObject.create("sid", "playlist1", "trackName"));
    trackObjects.add(TrackObject.create("sid", "playlist2", "trackName"));

    assertEquals(1, trackObjects.size());
  }
}
