package com.vseravno.playlistmanager.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TrackRepository extends JpaRepository<Track, Long> {

  @Query(
      "SELECT t FROM Track t "
          + "JOIN Account a ON t.accountId = a.id "
          + "WHERE a.userSid=:userSid")
  List<Track> findAllTracksByUserSpotifyId(@Param("userSid") String SpotifyId);
}
