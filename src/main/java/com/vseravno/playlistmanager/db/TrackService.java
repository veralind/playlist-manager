package com.vseravno.playlistmanager.db;

import com.vseravno.playlistmanager.PlaylistObject;
import com.vseravno.playlistmanager.TrackObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.vseravno.playlistmanager.api.UserAuth;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TrackService {
  private final TrackRepository trackRepository;
  private final PlaylistService playlistService;
  private final AccountService accountService;

  @Autowired
  public TrackService(
      TrackRepository trackRepository,
      PlaylistService playlistService,
      AccountService accountService) {
    this.trackRepository = trackRepository;
    this.playlistService = playlistService;
    this.accountService = accountService;
  }

  public boolean isTableEmpty(UserAuth auth) {
    return trackRepository.findAllTracksByUserSpotifyId(auth.getUserSid()).isEmpty();
  }



  public void updateTracks(Set<TrackObject> allTracks, UserAuth auth) throws Exception {
    //getting account id (foreign keys)
    Long accountId = accountService.getAccountId(auth);
    // getting playlists foreign keys
    Map<String, Long> playlistsSidsAndIds =
        playlistService.getPlaylistsSidsAndIds(auth);
    for (TrackObject tr : allTracks) {
      Track track = new Track();
      track.setAccountId(accountId);
      track.setTrackSid(tr.trackSid());
      track.setPlaylistId(playlistsSidsAndIds.get(tr.playlistSid()));
      track.setInserted(Timestamp.from(Instant.now()));
      trackRepository.save(track);
    }
  }

  // get user's tracks by user Spotify id
  public List<Track> getUserTracks(UserAuth auth){
    return trackRepository
        .findAllTracksByUserSpotifyId(auth.getUserSid());
  }
}
