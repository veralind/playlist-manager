package com.vseravno.playlistmanager.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface PlaylistRepository extends JpaRepository<Playlist, Long> {

  @Query(
      "SELECT p FROM Playlist p "
          + "JOIN Account a ON p.accountId = a.id "
          + "WHERE a.userSid=:userSid")
  List<Playlist> findAllUserPlaylistsByUserSid(@Param("userSid") String userSid);
}


