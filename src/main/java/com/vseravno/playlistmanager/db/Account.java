package com.vseravno.playlistmanager.db;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "account")
public class Account {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name="user_sid")
  private String userSid;

  @Column(name="access_token")
  private String accessToken;

  @Column(name="refresh_token")
  private String refreshToken;

  @Column(name="target_playlist_sid")
  private String targetPlaylistSid;

  private java.sql.Timestamp inserted;

  public Account(
      String userSid,
      String accessToken,
      String refreshToken,
      String targetPlaylistSid,
      Timestamp inserted) {
    this.userSid = userSid;
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
    this.targetPlaylistSid = targetPlaylistSid;
    this.inserted = inserted;
  }

  public Account() {}

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getTargetPlaylistSid() {
    return targetPlaylistSid;
  }

  public void setTargetPlaylistSid(String targetPlaylistSid) {
    this.targetPlaylistSid = targetPlaylistSid;
  }

  public String getUserSid() {
    return userSid;
  }

  public void setUserSid(String userSid) {
    this.userSid = userSid;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getRefreshToken() {
    return refreshToken;
  }

  public void setRefreshToken(String refreshToken) {
    this.refreshToken = refreshToken;
  }

  public Timestamp getInserted() {
    return inserted;
  }

  public void setInserted(Timestamp inserted) {
    this.inserted = inserted;
  }
}
