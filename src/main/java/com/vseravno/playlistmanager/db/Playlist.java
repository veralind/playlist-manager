package com.vseravno.playlistmanager.db;

import com.google.common.base.Objects;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "playlist")
public class Playlist {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name="account_id")
  private long accountId;

  @Column(name="playlist_sid")
  private String playlistSid;

  @Column(name="playlist_snapshot")
  private String playlistSnapshot;

  private java.sql.Timestamp inserted;

  public Playlist() {}

  public Playlist(long accountId, String playlistSid, String playlistSnapshot, Timestamp inserted) {
    this.accountId = accountId;
    this.playlistSid = playlistSid;
    this.playlistSnapshot = playlistSnapshot;
    this.inserted = inserted;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getAccountId() {
    return accountId;
  }

  public void setAccountId(long accountId) {
    this.accountId = accountId;
  }

  public String getPlaylistSid() {
    return playlistSid;
  }

  public void setPlaylistSid(String playlistSid) {
    this.playlistSid = playlistSid;
  }

  public String getPlaylistSnapshot() {
    return playlistSnapshot;
  }

  public void setPlaylistSnapshot(String playlistSnapshot) {
    this.playlistSnapshot = playlistSnapshot;
  }

  public Timestamp getInserted() {
    return inserted;
  }

  public void setInserted(Timestamp inserted) {
    this.inserted = inserted;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Playlist playlist = (Playlist) o;
    return id == playlist.id &&
        accountId == playlist.accountId &&
        Objects.equal(playlistSid, playlist.playlistSid) &&
        Objects.equal(playlistSnapshot, playlist.playlistSnapshot);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id, accountId, playlistSid, playlistSnapshot);
  }

  @Override
  public String toString() {
    return "Playlist{" +
        "id=" + id +
        ", accountId=" + accountId +
        ", playlistSid='" + playlistSid + '\'' +
        ", playlistSnapshot='" + playlistSnapshot + '\'' +
        '}';
  }
}
