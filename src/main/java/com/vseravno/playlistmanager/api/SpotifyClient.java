package com.vseravno.playlistmanager.api;

import com.google.gson.JsonArray;
import com.vseravno.playlistmanager.TrackObject;
import com.vseravno.playlistmanager.YAMLConfig;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.enums.ModelObjectType;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.AuthorizationCodeCredentials;
import com.wrapper.spotify.model_objects.specification.*;
import org.apache.hc.core5.http.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class SpotifyClient {

  private static final Logger logger = LoggerFactory.getLogger(SpotifyClient.class);

  private final SpotifyApi api;
  private final String userSid;
  private final int PLAYLIST_LIMIT = 50;
  private final int TRACK_LIMIT = 100;

  SpotifyClient(SpotifyApi api, String userSid) {
    this.api = api;
    this.userSid = userSid;
  }

  public static SpotifyClient create(String refreshToken, YAMLConfig yamlConfig) {
    try {
      SpotifyApi api =
          new SpotifyApi.Builder()
              .setClientId(yamlConfig.getClientId())
              .setClientSecret(yamlConfig.getClientSecret())
              .setRefreshToken(refreshToken)
              .build();
      // update tokens
      getNewTokens(api);
      String userSid = api.getCurrentUsersProfile().build().execute().getId();
      return new SpotifyClient(api, userSid);
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException("Failed to get create SpotifyClient object", e);
    }
  }

  public static SpotifyClient create(SpotifyApi api) {
    try {
      String userSid = api.getCurrentUsersProfile().build().execute().getId();
      return new SpotifyClient(api, userSid);
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException("Failed to get create SpotifyClient object", e);
    }
  }

  private static void getNewTokens(SpotifyApi api) {
    try {
      AuthorizationCodeCredentials tokens = api.authorizationCodeRefresh().build().execute();
      // set new tokens in the api
      api.setAccessToken(tokens.getAccessToken());
      // check if there is a new refresh token
      if (!(tokens.getRefreshToken() == null)) {
        api.setRefreshToken(tokens.getRefreshToken());
      }
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException("Failed to get new tokens", e);
    }
  }

  public Playlist createPlaylist(String playlistName) {
    try {
      return api.createPlaylist(userSid, playlistName)
          .collaborative(false)
          .public_(false)
          .description("A wild song appears.")
          .build()
          .execute();
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException("Failed to create the playlist for user " + userSid, e);
    }
  }

  public List<PlaylistSimplified> getUserPlaylists() {
    // first fetch
    Paging<PlaylistSimplified> simplifiedPaging = getPlaylists(0);
    int total = simplifiedPaging.getTotal();

    ArrayList<PlaylistSimplified> playlists =
        new ArrayList<>(Arrays.asList(simplifiedPaging.getItems()));

    for (int offset = PLAYLIST_LIMIT; offset < total; offset += PLAYLIST_LIMIT) {
      Paging<PlaylistSimplified> morePlaylists = getPlaylists(offset);
      playlists.addAll(Arrays.asList(morePlaylists.getItems()));
    }

    // removing possible duplicates (Spotify may send same playlist twice)
    Map<String, PlaylistSimplified> playlistsWithoutDuplicates =
        playlists.stream()
            .collect(Collectors.toMap(PlaylistSimplified::getId, pl -> pl, (id1, id2) -> id1));

    return new ArrayList<>(playlistsWithoutDuplicates.values());
  }

  public void uploadCustomPlaylistCoverImage(String imageData, String playlistSid) {
    try {
      api.uploadCustomPlaylistCoverImage(playlistSid).image_data(imageData).build().execute();
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException("Failed to upload playlist cover image for user " + userSid, e);
    }
  }

  public void addTracksToPlaylist(String playlistSid, List<TrackObject> tracks) {
    JsonArray jsonUris = new JsonArray();
    for (TrackObject track : tracks) {
      jsonUris.add(track.getUri());
    }
    try {
      api.addItemsToPlaylist(playlistSid, jsonUris).build().execute();
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      logger.error("Failed to add tracks to playlist {} for user {}", playlistSid, userSid, e);
    }
  }

  public Set<TrackObject> getAllTracksFromPlaylists(List<PlaylistSimplified> playlists) {
    HashSet<TrackObject> allTracks = new HashSet<>();
    for (PlaylistSimplified playlist : playlists) {
      try {
        getAllTracksFromOnePlaylist(playlist).stream()
            // rarely track can be null
            .filter(item -> item.getTrack() != null)
            .filter(item -> item.getTrack().getType().equals(ModelObjectType.TRACK))
            .map(item -> (Track) item.getTrack())
            // rarely track id can be null
            .filter(track -> track.getId() != null)
            .map(
                track -> {
                  String releaseDate = track.getAlbum().getReleaseDate();
                  String trackId = track.getId();
                  String trackName = track.getName();
                  return TrackObject.create(trackId, playlist.getId(), trackName, releaseDate);
                })
            .forEach(allTracks::add);
      } catch (Exception e) {
        logger.error(
            "Failed to fetch tracks from playlist {} for user {}", playlist.getId(), userSid, e);
      }
    }
    return allTracks;
  }

  private Paging<PlaylistSimplified> getPlaylists(int offset) {
    try {
      return api.getListOfUsersPlaylists(userSid)
          .limit(PLAYLIST_LIMIT)
          .offset(offset)
          .build()
          .execute();
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException("Failed to get list of user playlists for user " + userSid, e);
    }
  }

  public ArrayList<PlaylistTrack> getMaxAmountOfTracksOnce(
      PlaylistSimplified playlist, int offset) {
    String sid = playlist.getId();
    Paging<PlaylistTrack> temp = getTracks(sid, offset);
    return new ArrayList<>(Arrays.asList(temp.getItems()));
  }

  public boolean isUserActive(String targetPlaylist_sid) {
    String[] user = new String[] {userSid};
    if (targetPlaylist_sid == null || targetPlaylist_sid.isEmpty()) {
      return false;
    }
    try {
      Boolean[] isUserFollowingTargetPlaylist =
          api.checkUsersFollowPlaylist(userSid, targetPlaylist_sid, user).build().execute();
      return isUserFollowingTargetPlaylist[0];
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException(
          "Failed to check if user is following the playlist for user " + userSid, e);
    }
  }

  private ArrayList<PlaylistTrack> getAllTracksFromOnePlaylist(PlaylistSimplified playlist) {
    String playlistSid = playlist.getId();
    int total = playlist.getTracks().getTotal();

    // first fetch
    Paging<PlaylistTrack> pagingTracks = getTracks(playlistSid, 0);
    ArrayList<PlaylistTrack> playlistTracks =
        new ArrayList<>(Arrays.asList(pagingTracks.getItems()));

    for (int offset = TRACK_LIMIT; offset < total; offset += TRACK_LIMIT) {
      Paging<PlaylistTrack> moreTracks = getTracks(playlistSid, offset);
      playlistTracks.addAll(Arrays.asList(moreTracks.getItems()));
    }

    return playlistTracks;
  }

  private Paging<PlaylistTrack> getTracks(String playlistSid, int offset) {
    try {
      return api.getPlaylistsItems(playlistSid).limit(TRACK_LIMIT).offset(offset).build().execute();
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      logger.error("Failed to get tracks from playlist {}  for user {}", playlistSid, userSid, e);
      throw new RuntimeException();
    }
  }

  public UserAuth getUserAuth() {
    return new UserAuth(userSid, api.getAccessToken(), api.getRefreshToken());
  }
}
