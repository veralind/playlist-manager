package com.vseravno.playlistmanager;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.auto.value.AutoValue;
import org.springframework.lang.Nullable;

@AutoValue
public abstract class PlaylistObject {

  @JsonProperty("sid")
  public abstract String playlistSid();

  @JsonProperty("name")
  public abstract String playlistName();

  @JsonProperty("snapshot")
  public abstract String playlistSnapshot();

  @Nullable
  @JsonProperty("cover")
  public abstract String playlistCoverImage();

  @Nullable
  @JsonProperty("updated")
  public abstract Boolean updatedLastThreeMonths();

  @Nullable
  @JsonProperty("chosen")
  public abstract Boolean chosen();

  @JsonCreator
  public static PlaylistObject create(
      @JsonProperty("sid") String playlistSid,
      @JsonProperty("name") String playlistName,
      @JsonProperty("snapshot") String playlistSnapshot,
      @JsonProperty("cover") @Nullable String playlistCoverImage,
      @JsonProperty("updated") @Nullable Boolean updatedLastThreeMonths,
      @JsonProperty("chosen") @Nullable Boolean chosen) {
    return new AutoValue_PlaylistObject(
        playlistSid,
        playlistName,
        playlistSnapshot,
        playlistCoverImage,
        updatedLastThreeMonths,
        chosen);
  }
}
