package com.vseravno.playlistmanager;

import com.wrapper.spotify.SpotifyHttpManager;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.net.URI;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class YAMLConfig {
  private String clientId;
  private String clientSecret;
  private String redirectUri;
  private String updSecret;

  public String getUpdSecret() {
    return updSecret;
  }

  public String getClientId() {
    return clientId;
  }

  public String getClientSecret() {
    return clientSecret;
  }

  public URI getRedirectUri() {
    return SpotifyHttpManager.makeUri(redirectUri);
  }

  public void setUpdSecret(String updSecret) {
    this.updSecret = updSecret;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public void setClientSecret(String clientSecret) {
    this.clientSecret = clientSecret;
  }

  public void setRedirectUri(String redirectUri) {
    this.redirectUri = redirectUri;
  }
}
