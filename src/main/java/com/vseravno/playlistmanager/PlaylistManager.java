package com.vseravno.playlistmanager;

import com.google.common.collect.Lists;
import com.google.common.io.Resources;
import com.vseravno.playlistmanager.db.Playlist;
import com.vseravno.playlistmanager.db.Track;
import com.wrapper.spotify.model_objects.specification.Image;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.vseravno.playlistmanager.api.SpotifyClient;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

public class PlaylistManager {
  private static final Logger logger = LoggerFactory.getLogger(PlaylistManager.class);
  private final SpotifyClient client;
  private static final int TRACK_LIMIT = 100;
  private static final String PLAYLIST_NAME = "Unique playlist manager";
  private static final int DAYS_WITHOUT_UPDATE = 90;
  private final List<PlaylistSimplified> userPlaylists;

  public PlaylistManager(SpotifyClient client) {
    this.client = client;
    this.userPlaylists = client.getUserPlaylists();
  }

  public String createTargetPlaylist() throws IOException {
    com.wrapper.spotify.model_objects.specification.Playlist playlist =
        client.createPlaylist(PLAYLIST_NAME);
    String playlistSid = playlist.getId();
    addCoverImage(playlistSid);
    return playlistSid;
  }

  public void updateTargetPlaylist(String playlistSid, Set<TrackObject> newTracks) {
    // adding 100 tracks at a time
    Lists.partition(Lists.newArrayList(newTracks), TRACK_LIMIT)
        .forEach(
            tracksBatch -> {
              try {
                client.addTracksToPlaylist(playlistSid, tracksBatch);
                // just in case
                Thread.sleep(2000);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            });
  }

  public List<PlaylistObject> getUserPlaylists() {
    ArrayList<PlaylistObject> playlistObjects = new ArrayList<>();
    String userSid = client.getUserAuth().getUserSid();
    for (PlaylistSimplified playlist : userPlaylists) {
      String playlistOwnerSid = playlist.getOwner().getId();
      // ignore playlists owned by user
      if (!playlistOwnerSid.equals(userSid)) {
        String sid = playlist.getId();
        String name = playlist.getName();
        String snapshot = playlist.getSnapshotId();
        String playlistCoverImage = getPlaylistCoverImage(playlist);
        boolean updated = updatedInLastThreeMonths(playlist);
        playlistObjects.add(
            PlaylistObject.create(sid, name, snapshot, playlistCoverImage, updated, false));
      }
    }
    return playlistObjects;
  }

  public List<PlaylistObject> getUserPlaylists(List<Playlist> chosenPlaylists) {
    ArrayList<PlaylistObject> playlistObjects = new ArrayList<>();
    Set<String> chosenSids =
        chosenPlaylists.stream().map(Playlist::getPlaylistSid).collect(Collectors.toSet());
    String userSid = client.getUserAuth().getUserSid();
    for (PlaylistSimplified playlist : userPlaylists) {
      String playlistOwnerSid = playlist.getOwner().getId();
      // ignore playlists owned by user
      if (!playlistOwnerSid.equals(userSid)) {
        String sid = playlist.getId();
        String name = playlist.getName();
        String snapshot = playlist.getSnapshotId();
        String playlistCoverImage = getPlaylistCoverImage(playlist);
        boolean chosen = chosenSids.contains(sid);
        playlistObjects.add(
            PlaylistObject.create(sid, name, snapshot, playlistCoverImage, null, chosen));
      }
    }
    return playlistObjects;
  }

  public List<PlaylistObject> getUpdatedPlaylists(List<Playlist> playlists) {

    List<PlaylistObject> updatedPlaylists = new ArrayList<>();

    // getting names from playlists
    Map<String, String> sidsAndNames =
        userPlaylists.stream()
            .collect(Collectors.toMap(PlaylistSimplified::getId, PlaylistSimplified::getName));

    // getting possible new snapshots from playlists
    Map<String, String> sidsAndSnapshots =
        userPlaylists.stream()
            .collect(
                Collectors.toMap(PlaylistSimplified::getId, PlaylistSimplified::getSnapshotId));

    for (Playlist playlist : playlists) {
      String sid = playlist.getPlaylistSid();
      String name = sidsAndNames.get(sid);
      String oldSnapshot = playlist.getPlaylistSnapshot();
      String newSnapshot = sidsAndSnapshots.get(sid);
      // comparing and updating snapshots
      if (!oldSnapshot.equals(newSnapshot) && newSnapshot != null) {
        updatedPlaylists.add(PlaylistObject.create(sid, name, newSnapshot, null, null, null));
      }
    }
    return updatedPlaylists;
  }

  public Set<TrackObject> getNewTracks(List<PlaylistObject> updatedPlaylists, List<Track> dbFetch) {
    Set<TrackObject> tracksFromUpdatedPlaylists = getAllTracksFromPlaylists(updatedPlaylists);
    // get db tracks sids
    Set<String> dbSids = dbFetch.stream().map(Track::getTrackSid).collect(Collectors.toSet());
    // return tracks that are not present in the database
    return tracksFromUpdatedPlaylists.stream()
        .filter(t -> !dbSids.contains(t.trackSid()))
        .collect(Collectors.toSet());
  }

  private boolean updatedInLastThreeMonths(PlaylistSimplified playlist) {
    // minimizing calls to spotify by fetching 100 tracks at a time
    int amountOfAllTracks = playlist.getTracks().getTotal();
    int offset = 0;
    while (offset < amountOfAllTracks) {
      ArrayList<PlaylistTrack> tracks = client.getMaxAmountOfTracksOnce(playlist, offset);
      for (PlaylistTrack track : tracks) {
        LocalDate trackDate =
            track.getAddedAt().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        long days = ChronoUnit.DAYS.between(trackDate, LocalDate.now());
        if (days <= DAYS_WITHOUT_UPDATE) {
          return true;
        }
      }
      offset += TRACK_LIMIT;
    }
    return false;
  }

  public void initTargetPlaylistUpdate(Set<TrackObject> tracks, String playlistSid) {
    try{
    Set<TrackObject> recentlyReleasedTracks =
        tracks.stream()
            .filter(track -> track.releaseDate().length() == 10)
            .sorted(Comparator.comparing(TrackObject::releaseDate).reversed())
            .limit(10)
            .collect(Collectors.toSet());
    updateTargetPlaylist(playlistSid, recentlyReleasedTracks);
    }
    catch(Exception e){
      logger.error("Failed to get recent tracks for initial update for user {}", client.getUserAuth().getUserSid(), e);
    }
  }

  public Set<TrackObject> getAllTracksFromPlaylists(List<PlaylistObject> playlists) {
    // get ids from playlists
    Set<String> playlistSids =
        playlists.stream().map(PlaylistObject::playlistSid).collect(Collectors.toSet());
    // find playlists in simplified
    List<PlaylistSimplified> simplifiedPlaylists =
        userPlaylists.stream()
            .filter(playlist -> playlistSids.contains(playlist.getId()))
            .collect(Collectors.toList());

    return client.getAllTracksFromPlaylists(simplifiedPlaylists);
  }

  public List<PlaylistObject> filterNewChosenPlaylists(
      List<Playlist> dbPlaylists, List<PlaylistObject> chosenPlaylists) {

    Set<String> dbSids =
        dbPlaylists.stream().map(Playlist::getPlaylistSid).collect(Collectors.toSet());

    // if playlist is in chosen, but not in db, it's new
    return chosenPlaylists.stream()
        .filter(pl -> !dbSids.contains(pl.playlistSid()))
        .collect(Collectors.toList());
  }

  private String getPlaylistCoverImage(PlaylistSimplified playlist) {
    Image[] images = playlist.getImages();
    if (images == null || images.length == 0) {
      return "";
    }
    return images[0].getUrl();
  }

  private void addCoverImage(String playlistId) throws IOException {
    URL resource = Resources.getResource("cover.jpg");
    byte[] bytes = Resources.toByteArray(resource);
    String encodedImage = new String(Base64.getEncoder().encode(bytes), "UTF-8");
    client.uploadCustomPlaylistCoverImage(encodedImage, playlistId);
  }
}
